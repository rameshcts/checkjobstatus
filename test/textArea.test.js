let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let TextArea = require('../js/components/common/TextArea'); //my root-test lives in components/__tests__/, so this is how I require in my components.
let chai = require('chai');
let expect = chai.expect;

describe('textarea testcases', function () {
  before('before creating component', function() {
    this.TextAreaComp = TestUtils.renderIntoDocument(<TextArea />);
    this.textarea = TestUtils.findRenderedDOMComponentWithTag(this.TextAreaComp, 'textarea');
  });

  it('check rendered tagName', function() { 
    let renderedDOM = ReactDOM.findDOMNode(this.TextAreaComp);
    expect(renderedDOM.tagName).to.equal("TEXTAREA");
    expect(renderedDOM.classList.length).to.equal(1);
    expect(renderedDOM.classList[0]).to.equal("pe-textarea");
  });

  it('renders a textarea and value is empty', function () {
    expect(this.textarea).to.exist;
    expect(this.textarea.value).to.equal("");
    expect(this.textarea.rows).to.equal(4);
    expect(this.textarea.cols).to.equal(50);
    expect(this.textarea.placeholder).to.equal("");
    expect(this.textarea.maxLength).to.equal(300);
    expect(this.textarea.disabled).to.equal(false);
  });

  it('simulate change event and check value ', function () { 
    TestUtils.Simulate.change(this.textarea, {target: {value: 'Hello, world'}});
    //expect(this.textarea.value).to.equal('Hello, world');
    //expect(this.TextAreaComp.state.value).to.equal("Hello, world");
  });

  it('set state "value" and property value is still empty as it is a stateless component', function () {
    let TextAreaComp1 = TestUtils.renderIntoDocument(<TextArea/>);
    TextAreaComp1.setState({ value: "New state value" });
    let textarea1 = TestUtils.findRenderedDOMComponentWithTag(TextAreaComp1, 'textarea');
    expect(ReactDOM.findDOMNode(textarea1).getAttribute("value")).not.to.equal("New state value");
  });

  it('set property "value" and check state', function () {
    let TextAreaComp = TestUtils.renderIntoDocument(<TextArea value="sample"/>);
    let textarea = TestUtils.findRenderedDOMComponentWithTag(TextAreaComp, 'textarea');
    //expect(TextAreaComp.state.value).to.equal("sample");
    expect(textarea.value).to.equal('sample');
  });

  it('set property "placeholder" and check state', function () {
    let TextAreaComp = TestUtils.renderIntoDocument(<TextArea placeholder="enter data"/>);
    //expect(TextAreaComp.state.placeholder).to.equal("enter data");
    let textarea = TestUtils.findRenderedDOMComponentWithTag(TextAreaComp, 'textarea');
    expect(textarea.placeholder).to.equal('enter data');
    expect(ReactDOM.findDOMNode(textarea).getAttribute("placeholder")).to.equal('enter data');
  });

  it('set property "maxlength" and set value exceeding maxlength', function () {
    let TextAreaComp = TestUtils.renderIntoDocument(<TextArea maxLength="2"/>);
    //expect(TextAreaComp.state.maxLength).to.equal("2");
    let textarea = TestUtils.findRenderedDOMComponentWithTag(TextAreaComp, 'textarea');
    expect(textarea.maxLength).to.equal(2);
    expect(ReactDOM.findDOMNode(textarea).getAttribute("maxLength")).to.equal('2');
  });

  
  it('set property "readOnly" and check the attribute "readOnly"', function () {
    let TextAreaComp5 = TestUtils.renderIntoDocument(<TextArea readOnly={true} />);
    //expect(TextAreaComp5.state.readOnly).to.equal(true);
    let textarea5 = TestUtils.findRenderedDOMComponentWithTag(TextAreaComp5, 'textarea');
    expect(textarea5.readOnly).to.equal(true);
    //expect(ReactDOM.findDOMNode(textarea5).getAttribute("readOnly")).to.equal(true);
  });


  it('set property "disabled" and check the attribute "disabled"', function () {
    let TextAreaComp5 = TestUtils.renderIntoDocument(<TextArea disabled={true} />);
    //expect(TextAreaComp5.state.disabled).to.equal(true);
    let textarea5 = TestUtils.findRenderedDOMComponentWithTag(TextAreaComp5, 'textarea');
    expect(textarea5.disabled).to.equal(true);
    //expect(ReactDOM.findDOMNode(textarea5).getAttribute("disabled")).to.equal(true);
  });

  it('set property "required" and check the attribute "required"', function () {
    let TextAreaComp = TestUtils.renderIntoDocument(<TextArea required={true}/>);
    //expect(TextAreaComp.state.required).to.equal(true);
    let textarea = TestUtils.findRenderedDOMComponentWithTag(TextAreaComp, 'textarea');
    expect(textarea.required).to.equal(true);
    //expect(ReactDOM.findDOMNode(textarea).getAttribute("required")).to.equal(true);
  });

  it('set property "rows" and check the attribute "rows"', function () {
    let TextAreaComp = TestUtils.renderIntoDocument(<TextArea rows={5}/>);
    //expect(TextAreaComp.state.rows).to.equal(5);
    let textarea = TestUtils.findRenderedDOMComponentWithTag(TextAreaComp, 'textarea');
    expect(textarea.rows).to.equal(5);
    expect(ReactDOM.findDOMNode(textarea).getAttribute("rows")).to.equal("5");
  });

  it('set property "cols" and check the attribute "cols"', function () {
    let TextAreaComp = TestUtils.renderIntoDocument(<TextArea cols={100}/>);
    //expect(TextAreaComp.state.cols).to.equal(100);
    let textarea = TestUtils.findRenderedDOMComponentWithTag(TextAreaComp, 'textarea');
    expect(textarea.cols).to.equal(100);
    expect(ReactDOM.findDOMNode(textarea).getAttribute("cols")).to.equal("100");
  });

});





