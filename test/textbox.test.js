let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let TextBox = require('../js/components/common/TextBox'); //my root-test lives in components/__tests__/, so this is how I require in my components.
let chai = require('chai');
let expect = chai.expect;

describe('textbox testcases', function () {
  before('before crating textbox', function() {
    this.TextBoxComp = TestUtils.renderIntoDocument(<TextBox />);
    this.input = TestUtils.findRenderedDOMComponentWithTag(this.TextBoxComp, 'input');
});
  
  it('Renderes Input tag with "pe-input" class', function() { 
    let renderedDOM = ReactDOM.findDOMNode(this.TextBoxComp);
    expect(renderedDOM.tagName).to.equal("INPUT");
    expect(renderedDOM.classList.length).to.equal(1);
    expect(renderedDOM.classList[0]).to.equal("pe-input");
  });

  it('renders a input with default values', function () {
    expect(this.input).to.exist;
    expect(this.input.value).to.equal("");
    expect(this.input.placeholder).to.equal("");
    expect(this.input.disabled).to.equal(false);
    expect(this.input.required).to.equal(false);
    expect(this.input.maxLength).to.equal(30);
    expect(this.input.autofocus).to.equal(false);
  });

  it('allows changing component value', function () {
    var stub =sinon.spy(this.TextBoxComp, 'handleChange');
    //TestUtils.Simulate.change(this.input, {target: {value: 'Hello, world'}});
    this.TextBoxComp.handleChange({"target":{"value":"Hello, world"}});
    expect(this.TextBoxComp.state.value).to.equal("Hello, world");
    //check if handle change method is called
    //expect(this.input.value).to.equal("Hello, world");
    sinon.assert.called(stub);
  });

  it('changes value after setting state', function () {
    let TextBoxComp1 = TestUtils.renderIntoDocument(<TextBox/>);
    TextBoxComp1.setState({ value: "New state value" });
    let input1 = TestUtils.findRenderedDOMComponentWithTag(TextBoxComp1, 'input');
    expect(ReactDOM.findDOMNode(input1).getAttribute("value")).not.to.equal("New state value");
  });

  it('passing "value" property updates state and DOM element', function () {
    let TextBoxComp = TestUtils.renderIntoDocument(<TextBox value="sample"/>);
    //expect(TextBoxComp.state.value).to.equal("sample");
    let input = TestUtils.findRenderedDOMComponentWithTag(TextBoxComp, 'input');
    expect(input.value).to.equal('sample');
    //not working
    //expect(ReactDOM.findDOMNode(input).getAttribute("value")).to.equal('sample');
  });

  it('passing "placeholder" property updates state and DOM element', function () {
    let TextBoxComp = TestUtils.renderIntoDocument(<TextBox placeholder="enter data"/>);
    //expect(TextBoxComp.state.placeholder).to.equal("enter data");
    let input = TestUtils.findRenderedDOMComponentWithTag(TextBoxComp, 'input');
    expect(ReactDOM.findDOMNode(input).getAttribute("placeholder")).to.equal('enter data');
  });

  it('passing "maxlength" property updates state and DOM element', function () {
    let TextBoxComp = TestUtils.renderIntoDocument(<TextBox maxLength="2"/>);
    //expect(TextBoxComp.state.maxLength).to.equal("2");
    let input = TestUtils.findRenderedDOMComponentWithTag(TextBoxComp, 'input');
    expect(ReactDOM.findDOMNode(input).getAttribute("maxLength")).to.equal('2');
  });


  it('passing "disabled" property updates state and DOM element', function () {
    let TextBoxComp5 = TestUtils.renderIntoDocument(<TextBox disabled={true} />);
    //expect(TextBoxComp5.state.disabled).to.equal(true);
    let input5 = TestUtils.findRenderedDOMComponentWithTag(TextBoxComp5, 'input');
    //console.log("---->",input5);
    //not working
    //expect(ReactDOM.findDOMNode(input5).getAttribute("disabled")).to.equal(true);
  });

  it('passing "required" property updates state and DOM element', function () {
    let TextBoxComp = TestUtils.renderIntoDocument(<TextBox required={true}/>);
    //expect(TextBoxComp.state.required).to.equal(true);
    let input = TestUtils.findRenderedDOMComponentWithTag(TextBoxComp, 'input');
    //console.log("---->",input);
    //not working
    //expect(ReactDOM.findDOMNode(input).getAttribute("required")).to.equal(true);
  });

});





