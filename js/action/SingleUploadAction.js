// import UploadFilesApi from '../api/SingleUploadApi'

// export const selectPublisherData = (text) => {
//   return {
//     type: 'UPLOAD_FILES_DATA',
//     data: UploadFilesApi,
//     text
//   }
// }
/*
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *.
 *
 * LinkMetaDataActions
 */
import SingleUploadApi from '../api/SingleUploadApi'
import * as types from '../constants/MVMConstants'

  export function  fetchSUData (){
    return dispatch => {
      SingleUploadApi.get_SFU_Data().then(function(data){debugger;
        dispatch({
          type: types.SINGLEUPLOADDATA_GET,
          data : JSON.parse(data.text)
        })
      })
  }
}

  export function  saveMetaData (values){
    return dispatch => {
      MetaDataApi.save_QMD_Data(values).then(function(data){
        dispatch({
          type: types.SAVE_METADATA,
          QMD_Data : JSON.parse(data.text),values
        })
      })
  }
}
