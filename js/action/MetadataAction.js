/*
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *.
 *
 * LinkMetaDataActions
 */
import MetaDataApi from '../api/MetadataApi'
import * as types from '../constants/MVMConstants'

  export function  fetchMetaData (){
    return dispatch => {
      MetaDataApi.get_QMD_Data().then(function(data){debugger;
        dispatch({
          type: types.METADATA_GET,
          QMD_Data : JSON.parse(data.text)
        })
      })
  }
}

  export function  saveMetaData (values){
    return dispatch => {
      MetaDataApi.save_QMD_Data(values).then(function(data){
        dispatch({
          type: types.SAVE_METADATA,
          QMD_Data : JSON.parse(data.text),values
        })
      })
  }
}
