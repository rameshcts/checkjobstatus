/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
 "use strict";
 import React from 'react';
 import Heading from '../components/common/Heading';
 import MVMComponent from '../components/MVM/MVM';
 import {fetchMetaData, saveMetaData} from '../action/MetadataAction';
 import ReactTabs from 'react-tabs';
 import { connect } from 'react-redux'


const getSelectedValues = (dataArray) => {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }
  return [];
}

const mapStateToProps = (state) => {
    let metadata = getSelectedValues(state.Metadatareducers);
     if (metadata.contentTypeData) {
    return {
        suggestions: metadata.suggestions,
        contentTypeData : metadata.contentTypeData,
        audienceRolesData : metadata.audienceRolesData,
        difficultyLevelData : metadata.difficultyLevelData,
        knowledgeLevelData : metadata.knowledgeLevelData,
        alignmentTypeData : metadata.alignmentTypeData,
        "initialValues": {
        uuid : metadata.uuid,
        assTitle : metadata.assTitle,
        quesName : metadata.quesName,
        URI : metadata.uri,
        contentType : encodeURIComponent(JSON.stringify(metadata.contentTypeData[0])),
        audienceRole :  encodeURIComponent(JSON.stringify(metadata.audienceRolesData[0])),
        difficultLevel : encodeURIComponent(JSON.stringify( metadata.difficultyLevelData[0])),
        knowledgeLevel : encodeURIComponent(JSON.stringify( metadata.knowledgeLevelData[0])),
        alignType :  encodeURIComponent(JSON.stringify(metadata.alignmentTypeData[0])),
        enabObj : metadata.enabObj,
        timeReq : metadata.timeReq,
        desc : metadata.desc,
        tags: metadata.tags
      }
    }
  }else{
      return state.form.mvm;

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    componentWillMount(){
      dispatch(fetchMetaData())
    },
    onSave(values, dispatch){
      console.log(values);
      dispatch(saveMetaData(values));
    }
  }
}

const MetadataContainerConnect = connect(
  mapStateToProps,
  mapDispatchToProps
  )(MVMComponent)

  export default MetadataContainerConnect;
