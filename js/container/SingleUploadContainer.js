import { connect } from 'react-redux'
import { fetchSUData } from '../action/SingleUploadAction'
//import UploadFilesTab from '../components/UploadFiles/UploadFilesTab'
//import MetaDataTab from '../components/UploadFiles/MetaDataTab'
import SingleUploadComponent from '../components/SingleUploadComponent'



const getSelectedValues = (dataArray) => {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }

  return [];
}


const mapStateToProps = (state) => {debugger;
  let data = getSelectedValues(state.SingleUploadReducers)
  return {
    SUD:data,
    Title:data.Title,
    AudienceRoles: data.audienceRoleData,
    Publishers:data.publishersData,
    IntraLevels:data.intracityLevelData,
    DisciDoms:data.disciDomData,
    DiffiLevels:data.difficultyLevelData,
    LearningExpPros:data.learnExpProData,
    suggestions:data.suggestions,
     // "initialValues":{
      FileTypes:data.fileTypeData,
      BusinessUnits:data.businessUnitData,
      tags:data.tags,
      Description:data.Description
    //}
  }
}

 const handleTextChange = (obj) => {
    var key = obj.id;
    var value = obj.value;
    var stateObj = obj;
    stateObj[key]=value;
     this.setState(stateObj);
        if (this.props.handleChange) {
            this.props.handleChange(this.state);
        }
}

const handleChange = (obj) => {
    var key = obj.id;
    var value = obj.text;
    var stateObj = this.state;
    stateObj[key]=value;
        this.setState(stateObj);
        if (this.props.handleChange) {
            this.props.handleChange(this.state);
        }
}



const mapDispatchToProps = (dispatch) => {
  return {
   handleChange: handleChange,
   handleTextChange: handleTextChange,
   componentWillMount: function () {
      dispatch(fetchSUData());
   }
  }
}


const SingleUploadContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SingleUploadComponent)

export default SingleUploadContainer