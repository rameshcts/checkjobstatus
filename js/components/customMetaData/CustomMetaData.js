/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
"use strict";
import React from 'react';
import MetaDataList from './MetaDataList';
import Heading from './../common/Heading';

class CustomMetaData extends React.Component{
constructor(props) {
    super(props);
    this.displayName = 'CustomMetaData';
    this.addTask = this.addTask.bind(this);
    this.deleteTask = this.deleteTask.bind(this);
    
}
static propTypes= {
		items:React.PropTypes.array,
		count:React.PropTypes.number,
        title:React.PropTypes.string
}
static defaultProps= {

			items:[],
			count:0,
            title:"Contributor"

}
componentWillMount() {
        
}
componentDidMount() {
        
}
state= {
	items:this.props.items,
	count:this.props.items.length,
    title:this.props.title
}
deleteTask(e) {
        var taskIndex = parseInt(e.target.value, 10);
		var props = this.props;
		var items = [];
		props.items.map((task, index) =>
			{if(task.key === taskIndex){
                console.log("items to be removed is ",props.items[index]);
						props.items.splice(index, 1);
						items = props.items;
			}}
        );
		this.props.updateState({
            items: items
        });
}

addTask(e){
		var countVar = this.props.items.length+1;
        var itemsList = this.props.items;
        itemsList = itemsList.concat({"key":countVar});
		this.props.updateState({
            items: itemsList
        });
        e.preventDefault();
}
render(){
        return(
            <div>
               <MetaDataList items={this.props.items} deleteTask={this.deleteTask} />
                <form onSubmit={this.addTask}>
                    <button ClassNme="add"> <span className="pe-icon--plus-circle"></span>
                    <span>+</span></button>
                </form>
                
            </div>
        );
    }
};

module.exports = CustomMetaData;
