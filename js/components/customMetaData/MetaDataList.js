/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
"use strict";
import React from 'react';
import SelectBox from './../common/SelectBox';
import TextBox from './../common/TextBox';


var RightsInformation=[
  {"value":"Options", "text":"Select an option"},
  {"value":"21", "text":"option 1"},
  {"value":"22", "text":"option 2"},
  {"value":"23", "text":"option 3"},
  {"value":"24", "text":"option 4"}
]


class MetaDataList extends React.Component{
constructor(props) {
    super(props);
    this.displayName = 'MetaDataList';
    this.state = {
    demo:props.demo
    }
    
}
static propTypes= {
		items:React.PropTypes.array.isRequired,
}
static defaultProps= {
        items:[]
}
componentWillMount() {
        var items = this.props.items;
        this.props.items.map(function(task, taskIndex) {
            task.key = taskIndex+1;
        });
}
componentDidMount() {
        
}
state= {
			items:this.props.items	
}
render(){
    return <div className="sub-wrapper">
            {this.props.items.map((task, taskIndex) =>
                <div key={task.key}>
                    <SelectBox id = "demo" value = {this.state.demo}
                     defaultValue = {this.state.selectedValue}
                     options = {RightsInformation}>
                    </SelectBox>
                    <TextBox id="contr_name" required ={true} value ={this.state.contr_name} handleChange={this.handleTextChange} placeholder = "Add contributor full name"/>
                    <button className="delete" onClick={this.props.deleteTask} value={task.key}>
                     <span className="pe-icon--trash-o">-</span> 
                     </button>
                </div>

            )}
        </div>;
    }
};

module.exports = MetaDataList;
