/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
"use strict";
import React, { Component, PropTypes } from 'react';
import ReactTabs from 'react-tabs';
import UploadFilesTab from './UploadFiles/UploadFilesTab'; 
import MetaDataTab from './UploadFiles/MetaDataTab'; 



var Tab = ReactTabs.Tab;
var Tabs = ReactTabs.Tabs;
var TabList = ReactTabs.TabList;
var TabPanel = ReactTabs.TabPanel;

/** Upload files component class */

class SingleUploadComponent extends React.Component{

  constructor(props) {debugger;
    super(props);
    this.handleChange = props.handleChange.bind(this);
    this.componentWillMount = props.componentWillMount;
  }

/** Render method */
render(){
    	return(
    			<div>
          <Tabs>
          <TabList>
          <Tab>Upload Files</Tab>
          <Tab>Meta Data</Tab>
        </TabList>
        <TabPanel>
        <UploadFilesTab data={this.props.SUD} />
        </TabPanel>
        <TabPanel>
          <MetaDataTab data={this.props.SUD}/>
        </TabPanel>
          </Tabs>
    			</div>

    		)
    }
};

// UploadFilesComponent.propTypes = { 

// }

module.exports = SingleUploadComponent;