import {defineMessages} from 'react-intl';
export const messages = defineMessages({
	Question_Keyword_Tags: {
        id: 'Question_Keyword_Tags',
        defaultMessage: 'Question Keyword Tags'
    },
    MVM_Data: {
        id: 'MVM_Data',
        defaultMessage: 'Minimally Viable Metadata'
    },
    MVM_UUID: {
    	id: 'MVM_UUID',
    	defaultMessage: 'UUID'
    },
    Assesment_Title: {
    	id:'Assesment_Title',
    	defaultMessage: 'Assesment Title'
    },
    Question_Name: {
    	id:'Question_Name',
    	defaultMessage: 'Question Name'
    },
    MVM_URI: {
    	id:'MVM_URI',
    	defaultMessage: 'URI'
    },
    Content_Type: {
    	id:'Content_Type',
    	defaultMessage: 'Content Type'
    },
    Audience_Role: {
    	id:'Audience_Role',
    	defaultMessage: 'Audience Role'
    },
    Difficult_Level: {
    	id:'Difficult_Level',
    	defaultMessage: 'Difficult Level'
    },
    Knowledge_Level: {
    	id:'Knowledge_Level',
    	defaultMessage: 'Knowledge Level'
    },
    Alignment_Type: {
    	id:'Alignment_Type',
    	defaultMessage: 'Alignment Type'
    },
    Enable_Objective: {
    	id: 'Enable_Objective',
    	defaultMessage: 'Enabling Objective'
    },
    Time_Required: {
    	id: 'Time_Required',
    	defaultMessage: 'Time Requried'
    },
    MVM_Desc: {
    	id: 'MVM_Desc',
    	defaultMessage: 'Description'
    },
    MVM_Save_Button:{
    	id:'MVM_Save_Button',
    	defaultMessage: 'Save'
    }
})