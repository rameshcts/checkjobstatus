/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
"use strict";
import React, {Component, PropTypes} from 'react';
import Label from'./../common/Label';
import TextBox from './../common/TextBox';
import Heading from './../common/Heading';
import SelectBox from './../common/SelectBox';
import TextArea from './../common/TextArea';
import TagElem from './../common/TagElem';
import TimePicker from'./../common/TimePicker';
import {reduxForm} from 'redux-form';
import FormMessages from 'redux-form-validation';
import {generateValidation} from 'redux-form-validation';
import {injectIntl, intlShape} from 'react-intl';
import {messages} from './MVMDefaultMessages';
import {fetchMetaData, saveMetaData} from '../../action/MetadataAction';
export const fields = ['uuid','assTitle', 'qName', 'uri', 'contentType', 'audienceRole',
                       'difficultLevel','knowledgeLevel','alignType', 'enableObj',
                       'timeReq','desc','tags'];
var validations = {
     qName: {
       required: true
     },
     'assTitle': false,
     'uuid': false,
     'uri': false,
     'contentType': false,
     'audienceRole': false,
     'difficultLevel': false,
     'knowledgeLevel': false,
     'alignType': false,
     'enableObj': false,
     'timeReq': false,
     'desc': false,
     'tags': false
};

class MVMComponent extends React.Component{
  static PropTypes = {
        intl: intlShape.isRequired
    }

constructor(props) {
    super(props);
    this.displayName = 'MVMComponent';
    this.componentWillMount = props.componentWillMount;
    this.onSave = props.onSave;

     this.state = {
      contentTypeData: [],
      audienceRolesData: [],
      difficultyLevelData: [],
      knowledgeLevelData: [],
      alignmentTypeData: []     
    }
}

componentWillReceiveProps (nextProps) {
    if (nextProps.contentTypeData) {
      this.state.contentTypeData = nextProps.contentTypeData;
    }
    if (nextProps.audienceRolesData) {
      this.state.audienceRolesData = nextProps.audienceRolesData;
    }
     if (nextProps.difficultyLevelData) {
      this.state.difficultyLevelData = nextProps.difficultyLevelData;
    }
    if (nextProps.knowledgeLevelData) {
      this.state.knowledgeLevelData = nextProps.knowledgeLevelData;
    }   
    if (nextProps.alignmentTypeData) {
      this.state.alignmentTypeData = nextProps.alignmentTypeData;
    }     
}
 
onBlur (e) {
  return true;
}

onChange (e) {
  return true;
}

_onChange(e){
        var val = e.target.value,
            name = e.target.name
        for(var key in this.state.fields){
            var field = this.state.fields[key]
            if(field.name == name){
                field.value = val
            }
        }
        this.setState(this.state)
    }

render() {
  const {formatMessage} = this.props.intl;
  var self = this
  const contentTypeData = this.state.contentTypeData || [];
  const audienceRolesData = this.state.audienceRolesData || [];
  const difficultyLevelData = this.state.difficultyLevelData || [];
  const knowledgeLevelData = this.state.knowledgeLevelData || [];
  const alignmentTypeData = this.state.alignmentTypeData || [];
  const{
    fields : {uuid, assTitle,qName,uri,contentType,audienceRole,difficultLevel,
              knowledgeLevel,alignType,enableObj,timeReq,desc,tags},
    handleSubmit
  }= this.props;

    return (
            <form onSubmit={handleSubmit(this.onSave)}>
            <div className="pe-updatemetadata"> 
            <section>
            <div>
              <h2>{formatMessage(messages.Question_Keyword_Tags)}</h2>
            </div>
            <div className="pe-input pe-input--horizontal" >
              <TagElem suggestions={this.props.suggestions} tags={tags.value}/>
            </div>
            </section>
            <section>
            <div>
              <h2>{formatMessage(messages.MVM_Data)}</h2>
            </div>
                <div className="pe-input pe-input--horizontal" >
                    <Label for="UUID" text={formatMessage(messages.MVM_UUID)}/>
                    <TextBox value={uuid} disabled={true} placeholder="uuid" />
                </div>
                < div className="pe-input pe-input--horizontal" >
                    <Label for ="AssignmentTitle" text={formatMessage(messages.Assesment_Title)}/>
                    <TextBox value={assTitle} disabled={true} placeholder="assTitle"/>
                </div>
                <div className="pe-input pe-input--horizontal navcontainer" >
                    <Label for ="Question Name" text={formatMessage(messages.Question_Name)}/>
                    <TextBox required={true} value = {qName} placeholder="Add a descriptive Name"/>
                    <FormMessages tagName="ul" errorCount="1" field={qName}>
                            <li when="required">
                                  Error:required field
                            </li>
                         </FormMessages>
                </div>

                <div className="pe-input pe-input--horizontal" >
                    <Label for ="URI" text={formatMessage(messages.MVM_URI)}/>
                    <TextBox value = {uri} placeholder="Add a canonical URI for Asset"/>
                </div>

                <div className="pe-input pe-input--horizontal">
                    <Label for ="ContentType" text={formatMessage(messages.Content_Type)}/>
                    <SelectBox id="contentType" value={contentType} onChange= {this.onChange}   onBlur= {this.onBlur} options={contentTypeData}/>                         
                </div>
                
                <div className="pe-input pe-input--horizontal">
                    <Label for ="Audience Role" text={formatMessage(messages.Audience_Role)}/>
                    <SelectBox id="audienceRole" value={audienceRole} onChange= {this.onChange}   onBlur= {this.onBlur} options={audienceRolesData}/>                       
                </div>

                <div className="pe-input pe-input--horizontal">
                    <Label for ="Difficulty Level" text={formatMessage(messages.Difficult_Level)}/>
                    <SelectBox id="difficultLevel" value={difficultLevel} onChange= {this.onChange}   onBlur= {this.onBlur} options={difficultyLevelData}/>
                </div>

                <div className="pe-input pe-input--horizontal">
                    <Label for ="Knowledge Level" text={formatMessage(messages.Knowledge_Level)}/>
                    <SelectBox id="knowledgeLevel"  value={knowledgeLevel} onChange= {this.onChange}   onBlur= {this.onBlur} options={knowledgeLevelData}/>
                </div>

                 <div className="pe-input pe-input--horizontal">
                    <Label for ="Alignment Type" text={formatMessage(messages.Alignment_Type)}/>
                    <SelectBox id="alignmentType"  value={alignType} onChange= {this.onChange}   onBlur= {this.onBlur} options={alignmentTypeData}/>
                </div>

                <div className="pe-input pe-input--horizontal">
                    <Label for="EnablingObejctive" text={formatMessage(messages.Enable_Objective)}/>
                    <TextBox value = {enableObj} placeholder="Add enabling obejctive URI"/>
                </div>
                <div className="pe-input pe-input--horizontal">
                  <Label for="TimeRequired" text={formatMessage(messages.Time_Required)}/>
                  <TimePicker required={false} id="timeReq" value={timeReq} onChange={self._onChange.bind(self)}/>
                </div>
                <div className="pe-input pe-input--horizontal">
                    <Label for="Description" text={formatMessage(messages.MVM_Desc)}/>
                    <TextArea id="description" value={desc} placeholder = "Description goes here">
                    </TextArea>
                </div>

                <div className="pe-input pe-input--horizontal">
                    <button onClick={handleSubmit(this.onSave)}>
                    {formatMessage(messages.MVM_Save_Button)}
                    </button>
                </div>
            </section>
            </div>
            </ form>
        )
    }
};

MVMComponent = reduxForm({
    form: 'mvm',
     fields: ['uuid', 'assTitle','qName','uri','contentType','audienceRole','difficultLevel',
              'knowledgeLevel','alignType','enableObj','timeReq','desc','tags'],
     ...generateValidation(validations)
  })(MVMComponent);

//module.exports = MVMComponent;
export default injectIntl(MVMComponent);