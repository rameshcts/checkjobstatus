/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
"use strict";
import React, { Component, PropTypes } from 'react';
import ReactTabs from 'react-tabs';
import TextBox from '../common/TextBox';
import Label from '../common/Label';
import TextArea from '../common/TextArea';
import SelectBox from '../common/SelectBox';
import RadioGroup from '../common/RadioBtn';
import CustomMetaData from '../customMetaData/CustomMetaData';
import {reduxForm} from 'redux-form';
import FormMessages from 'redux-form-validation';
import {generateValidation} from 'redux-form-validation';


class MetaDataTab extends React.Component{

  constructor(props) {
    super(props);
    // this.handleChange = props.handleChange.bind(this);
    //  this.componentWillMount = props.componentWillMount;
  }

render(){
  const {
        Title,BusinessUnits, Publishers,AudienceRoles,LearningExpPros,IntraLevels,DisciDoms,DiffiLevels
      } = this.props 
    return(
    <div>
    <div className='TabFullRow'>
    <div className='TabLeftRow'>
    <div className='TabFullRow'>
    <Label for="BusinessUnit" text="BusinessUnit" lblclass="UploadFilesTablabel" />
    <SelectBox id="BusinessUnit" onChange= {this.onChange} selectboxclass="UploadFilesSelectBox"   onBlur= {this.onBlur} options={this.props.data.businessUnitData}/>                         
    </div>
    </div>
    <div className='TabRightRow'>
    <div className='TabFullRow'>
    <Label for="Publisher" text="Publisher" lblclass="UploadFilesTablabel" />
    <SelectBox id="Publisher" onChange= {this.onChange} selectboxclass="UploadFilesSelectBox"   onBlur= {this.onBlur} options={this.props.data.publishersData}/>                         
    </div>
    </div>
    </div>
    <div className='TabFullRow'>
    <div className='TabLeftRow'>
    <div className='TabFullRow'>
    <Label for="AudienceRole" text="Audience Role" lblclass="UploadFilesTablabel" />
    <SelectBox id="AudienceRole" onChange= {this.onChange} selectboxclass="UploadFilesSelectBox"   onBlur= {this.onBlur} options={this.props.data.audienceRoleData}/>                         
    </div>
    </div>
    <div className='TabRightRow'>
    <div className='TabFullRow'>
    <Label for="LearningExpPro" text="LearningExperience/Program" lblclass="UploadFilesTablabel" />
    <SelectBox id="LearningExpPro" onChange= {this.onChange} selectboxclass="UploadFilesSelectBox"   onBlur= {this.onBlur} options={this.props.data.learnExpProData}/>                         
    </div>
    </div>
    </div>
    <div className='TabFullRow'>
    <div className='TabLeftRow'>
    <div className='TabFullRow'>
    <Label for="IntraLevel" text="Intracity Level" lblclass="UploadFilesTablabel" />
    <SelectBox id="IntraLevel" onChange= {this.onChange} selectboxclass="UploadFilesSelectBox"   onBlur= {this.onBlur} options={this.props.data.intracityLevelData}/>                         
    </div>
    </div>
    <div className='TabRightRow'>
    <div className='TabFullRow'>
    <Label for="DisciDom" text="Discipline/Domain" lblclass="UploadFilesTablabel" />
    <SelectBox id="DisciDom" onChange= {this.onChange} selectboxclass="UploadFilesSelectBox"   onBlur= {this.onBlur} options={this.props.data.disciDomData}/>                         
    </div>
    </div>
    </div>
    <div className='TabFullRow'>
    <div className='TabLeftRow'>
    <div className='TabFullRow'>
    <Label for="DiffiLevel" text="Difficulty Level" lblclass="UploadFilesTablabel" />
    <SelectBox id="DiffiLevel" onChange= {this.onChange} selectboxclass="UploadFilesSelectBox"   onBlur= {this.onBlur} options={this.props.data.difficultyLevelData}/>                         
    </div>
    </div>
    </div>
    </div>
    		)
    }
};



module.exports = MetaDataTab;