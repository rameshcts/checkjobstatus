/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
"use strict";
import React, { Component, PropTypes } from 'react';
import FileInput from '../common/ChooseFile';
import Label from '../common/Label';
import TextBox from '../common/TextBox';
import TextArea from '../common/TextArea';
import SelectBox from '../common/SelectBox';
import TagElem from '../common/TagElem';


/** Upload files component class */

class UploadFilesTab extends React.Component{

  constructor(props) {
    super(props);
    // this.handleChange = props.handleChange.bind(this);
    // this.componentWillMount = props.componentWillMount;
  }

/** Render method */
render(){
   const {
        Title,FileTypes=[], tags
      } = this.props 
    	return(
    	<div>
        <div className='TabContainer'>
        <div className='TabFullRow'>
        <FileInput accept=".png,.gif,.jpg" />
        </div>
       <div className='TabFullRow'>
        <div className='TabLeftRow'>
        <div className='TabFullRow'>
        <Label for="Title" text="Title(Required)" lblclass="UploadFilesTablabel" />
        <TextBox id="Title" textboxclass="UploadFilesTextBox" value={this.props.data.Title} handleChange={this.props.handleTextChange}/>
        </div>
        </div>
         <div className='TabRightRow'>
         <div className='TabFullRow'>
       <Label for ="FileType" text="FileType" lblclass="UploadFilesTablabel"/>
       <SelectBox id="FileType" onChange= {this.onChange} selectboxclass="UploadFilesSelectBox"   onBlur= {this.onBlur} options={this.props.data.fileTypeData}/>                         
        </div>
         </div>
         </div>
        <div className='TabLeftRow'>
        <div className='TabFullRow'>
        <Label for="Description" text="Description" lblclass="UploadFilesTablabel" />
        <TextArea value={this.props.data.Description}  textareaclass="UploadFilesTextArea" id="descId" placeholder="Description"/>        </div>
        </div>
        </div>
         <div className='TabRightRow'>
         <div className='TabFullRow'>
       <Label for="RelEmails" text="Related Emails" lblclass="UploadFilesTablabel" />
        <TextBox id="RelEmails" value={this.props.data.relatedEmails} textboxclass="UploadFilesTextBox" />
        </div>
         </div>
         <div className='TabFullRow'>
        <Label for="keywords" text="Keywords" lblclass="UploadFilesTablabel" />
        <TagElem suggestions={this.props.data.suggestions} tags={this.props.data.tags}/>
         </div>
         <div className='TabFullRow'>
         <div className='TabLeftRow'>
        <div className='TabFullRow'>
        <Label for="RefId" text="Reference Id" lblclass="UploadFilesTablabel" />
        <TextBox id="RefId" value={this.props.data.referenceId} textboxclass="UploadFilesTextBox" handleChange={this.props.handleTextChange}/>
        </div>
        </div>
         <div className='TabRightRow'>
         <div className='TabFullRow'>
       <Label for="PlayId" text="Playist Id" lblclass="UploadFilesTablabel" />
        <TextBox id="PlayId" value={this.props.data.playistId} textboxclass="UploadFilesTextBox" handleChange={this.props.handleTextChange}/>
        </div>
         </div>
         </div>
    	</div>

    		)
    }
};

// UploadFilesComponent.propTypes = { 

// }

module.exports = UploadFilesTab;