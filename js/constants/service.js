var baseUrl = "http://localhost:3000/";

var service = {
	questionMetaData: baseUrl + 'questionMetaData',
	saveQuestionMetaData: baseUrl + 'saveQuestionMetaData',
	singleUploadData:baseUrl + 'singleUploadData'
}

module.exports = service;