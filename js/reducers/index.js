import { combineReducers } from 'redux'
import Metadatareducers from './Metadatareducers'
import SingleUploadReducers from './SingleUploadReducers'

import {reducer as formReducer} from 'redux-form';

const uploadFilesData = combineReducers({
	Metadatareducers, 
	form: formReducer,
  	SingleUploadReducers
 
});

export default uploadFilesData
