import { METADATA_GET, SAVE_METADATA } from '../constants/MVMConstants'

let initilizeValues = [{
  uuid : ' ' , 
  assTitle:' ',
  quesName:' ',
  uri:' ',
  enabObj:'' ,
  contentTypeData: [],
  audienceRolesData: [],
  difficultyLevelData: [],
  knowledgeLevelData: [],
  alignmentTypeData: [],
  timeReq:'' ,
  desc:'',
  tags: []
}]

const Metadatareducers = (state = initilizeValues, action)=>{
  switch(action.type) {
    
    case 'METADATA_GET':
    return[
    ...state, action.QMD_Data
    ]
    break;
    
    case 'SAVE_METADATA':
    return[
    ...state, action.values
    ]
    break;
    
    default:
    return state
      
    }
  }

  export default Metadatareducers;